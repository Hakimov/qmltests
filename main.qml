import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Window {
    id : mainWindow
    visible: true
    width: 640
    height: 480
    minimumHeight: 480
    minimumWidth: 640
    color: "white"

    title: qsTr("Test")

    Image {
        id: light
        source: "/imgs/l1.png"
        fillMode: Image.PreserveAspectFit

        height: mainWindow.height * 0.8

        y: mainWindow.height - light.height
        x: mainWindow.width / 2 - light.width / 2
        Layout.column: 2
    }

    Button {
        id : button1
        text : "Button1"
        width: mainWindow.width/8; height: mainWindow.height/10
        x: 0;  y: 0
    }

    Button {
        id : button2
        text : "Button2"
        width: mainWindow.width/8; height: mainWindow.height/10
        x: mainWindow.width/8;  y: 0
    }

    Button {
        id : button3
        text : "Button3"
        width: mainWindow.width/8; height: mainWindow.height/10
        x: mainWindow.width/8*2;  y: 0
    }

    Button {
        id : button4
        text : "Button4"
        width: mainWindow.width/8; height: mainWindow.height/10
        x: mainWindow.width/8*3;  y: 0
    }

    Button {
        id : button5
        text : "Button5"
        width: mainWindow.width/8; height: mainWindow.height/10
        x: mainWindow.width/8*4;  y: 0
    }

    Button {
        id : button6
        text : "Button6"
        width: mainWindow.width/8; height: mainWindow.height/10
        x: mainWindow.width/8*5;  y: 0
    }

    Button {
        id : button7
        text : "Button7"
        width: mainWindow.width/8; height: mainWindow.height/10
        x: mainWindow.width/8*6;  y: 0
    }

    Button {
        id : button8
        text : "Button8"
        width: mainWindow.width/8; height: mainWindow.height/10
        x: mainWindow.width/8*7;  y: 0
    }
}
